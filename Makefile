default: up

up:
	docker compose up -d --pull always

stop:
	make kill

kill:
	docker compose kill

down:
	make kill
	docker compose down --remove-orphans -v

build:
	docker compose up -d --pull always --build

logs:
	docker compose logs

prune:
	make down
	sudo rm -fr db

shell:
	docker compose exec -it wordpress /bin/bash


wp:
	docker compose run --rm -it wp /bin/bash

install:
	make prune
	make
	sleep 18
	docker compose run --rm wp wp core install --url=http://localhost --title=wp --admin_user=admin --admin_email=admin@admin.admin --admin_password=admin --locale=es_ES